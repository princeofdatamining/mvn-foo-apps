package com.png0.foo.web;

import static org.junit.Assert.*;
import org.junit.Test;

import com.png0.foo.lib.*;

public class FooWebTest {

    @Test
    public void testMain() {
        assertTrue((new Foo()).getLibName().startsWith("Foo"));
    }

}
