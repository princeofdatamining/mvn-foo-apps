package com.png0.foo.web;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import com.png0.foo.lib.*;

public class FooWeb extends HttpServlet {

    public void doGet(HttpServletRequest req,
                      HttpServletResponse response)
        throws IOException, ServletException
    {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.println(new Foo().getLibName());
    }

}