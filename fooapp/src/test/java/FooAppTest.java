package com.png0.foo.app;

import static org.junit.Assert.*;
import org.junit.Test;

import com.png0.foo.lib.*;

public class FooAppTest {

    @Test
    public void testMain() {
        assertTrue((new Foo()).getLibName().startsWith("Foo"));
    }

}
