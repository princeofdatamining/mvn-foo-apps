package com.png0.foo.lib;

import static org.junit.Assert.*;
import org.junit.Test;

public class FooTest {

    @Test
    public void testLibName() {
        assertTrue(new Foo().getLibName().startsWith("Foo"));
    }

}
